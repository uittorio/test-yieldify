import { PhysicBuilder } from "../../core/physic/physicBuilder";
import { Physic } from "../../core/physic/physic";

export class PhysicBuilderStub implements PhysicBuilder {
	withElasticity: (elasticity: number) => PhysicBuilder = jasmine.createSpy("withElasticity").and.returnValue(this);
	withFriction: (friction: number) => PhysicBuilder = jasmine.createSpy("withFriction").and.returnValue(this);
	withGravity: (gravity: number) => PhysicBuilder = jasmine.createSpy("withGravity").and.returnValue(this);
	withSpeed: (speed: number) => PhysicBuilder = jasmine.createSpy("withSpeed").and.returnValue(this);
	withAngle: (angle: number) => PhysicBuilder = jasmine.createSpy("withAngle").and.returnValue(this);
	build: () => Physic = jasmine.createSpy("buildInView");
}