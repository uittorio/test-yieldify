import { StandardPhysic } from "../../core/physic/standardPhysic";
import { BallStub } from "../ball/ball.stub";
import { Shape } from "../../core/shape/shape";

describe("StandardPhysic", () => {
	let standardPhysic: StandardPhysic;
	
	function moveTimes(shape: Shape, times: number) {
		for (let i= 0; i < times; i++) {
			standardPhysic.move(shape);
		}
	}
	
	beforeEach(() => {
		standardPhysic = StandardPhysic.create();
		standardPhysic.setFriction(0.018);
		standardPhysic.setElasticity(0.5);
		standardPhysic.setGravity(0.2);
		standardPhysic.setSpeed(6);
		standardPhysic.setVectorAngle(45);
	});
	
	describe("when moving a shape with bottom left angle", () => {
		let previousY: number,
			previousX: number,
			shape: Shape;
		
		beforeEach(() => {
			shape = new BallStub();
			shape.x = 200;
			shape.y = 200;
			previousY = shape.y;
			previousX = shape.x;
			standardPhysic.setVectorAngle(120);
			standardPhysic.move(shape);
			moveTimes(shape, 3000);
		});
		
		it('should move down the vertical position', () => {
			expect(shape.y > previousY).toBe(true);
		});
		
		it('should move the horizontal position', () => {
			expect(shape.x < previousX).toBe(true);
		})
	});
	
	describe("when moving a shape with top right angle", () => {
		let previousY: number,
			previousX: number,
			shape: Shape;
		
		beforeEach(() => {
			shape = new BallStub();
			shape.x = 200;
			shape.y = 200;
			previousY = shape.y;
			previousX = shape.x;
			standardPhysic.setVectorAngle(300);
			standardPhysic.move(shape);
		});
		
		it('should initially move up the vertical position', () => {
			expect(shape.y < previousY).toBe(true);
		});
		
		it('should move right the horizontal position', () => {
			expect(shape.x > previousX).toBe(true);
		});
		
		describe("when finishing moving", () => {
			beforeEach(() => {
				moveTimes(shape, 3000);
			});
			
			it('should move down the vertical position', () => {
				expect(shape.y > previousY).toBe(true);
			});
		});
	});
	
	describe("when moving a shape with bottom right angle", () => {
		let previousY: number,
			previousX: number,
			shape: Shape;
		
		beforeEach(() => {
			shape = new BallStub();
			shape.x = 200;
			shape.y = 200;
			previousY = shape.y;
			previousX = shape.x;
			standardPhysic.setVectorAngle(40);
			standardPhysic.move(shape);
		});
		
		it('should initially move down the vertical position', () => {
			expect(shape.y > previousY).toBe(true);
		});
		
		it('should move right the horizontal position', () => {
			expect(shape.x > previousX).toBe(true);
		});
		
		describe("when finishing moving", () => {
			beforeEach(() => {
				moveTimes(shape, 3000);
			});
			
			it('should move down the vertical position', () => {
				expect(shape.y > previousY).toBe(true);
			});
		});
	});
	
	describe("when moving a shape with top left angle", () => {
		let previousY: number,
			previousX: number,
			shape: Shape;
		
		beforeEach(() => {
			shape = new BallStub();
			shape.x = 200;
			shape.y = 200;
			previousY = shape.y;
			previousX = shape.x;
			standardPhysic.setVectorAngle(220);
			standardPhysic.move(shape);
		});
		
		it('should initially move up the vertical position', () => {
			expect(shape.y < previousY).toBe(true);
		});
		
		it('should move left the horizontal position', () => {
			expect(shape.x < previousX).toBe(true);
		});
		
		describe("when finishing moving", () => {
			beforeEach(() => {
				moveTimes(shape, 3000);
			});
			
			it('should move down the vertical position', () => {
				expect(shape.y > previousY).toBe(true);
			});
		});
	});
	
	describe("when the shape is already at the bottom", () => {
		let previousY: number,
			previousX: number,
			shape: Shape;
		
		beforeEach(() => {
			shape = new BallStub();
			
			standardPhysic.setVectorAngle(45);
			
			shape.y = 200;
			shape.x = 0;
			(shape.isOutsideBottomView as jasmine.Spy).and.returnValue(true);
			previousY = shape.y;
			previousX = shape.x;
			standardPhysic.move(shape);
		});
		
		it("should position the shape to the bottom", () => {
			expect(shape.positionToBottomView).toHaveBeenCalledWith();
		});
		
		describe("when moving again", () => {
			beforeEach(() => {
				previousY = shape.y;
				(shape.isOutsideBottomView as jasmine.Spy).and.returnValue(false);
				standardPhysic.move(shape);
			});
			
			it('should bounce up', () => {
				expect(shape.y < previousY).toBe(true);
			});
		});
	});
});