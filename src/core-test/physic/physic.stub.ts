import { Physic } from "../../core/physic/physic";
import { Shape } from "../../core/shape/shape";

export class PhysicStub implements Physic {
	public move: (shape: Shape) => void = jasmine.createSpy("");
	public setElasticity: (elasticity: number) => void = jasmine.createSpy("");
	
	public setFriction: (friction: number) => void = jasmine.createSpy("");
	
	public setGravity: (gravity: number) => void = jasmine.createSpy("");
	
	public setSpeed: (speed: number) => void = jasmine.createSpy("");
	
	public setVectorAngle: (angle: number) => void = jasmine.createSpy("");
}