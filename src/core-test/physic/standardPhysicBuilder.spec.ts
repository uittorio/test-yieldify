import { StandardPhysicBuilder } from "../../core/physic/standardPhysicBuilder";
import { StandardPhysic } from "../../core/physic/standardPhysic";
import { PhysicStub } from "./physic.stub";

describe("StandardPhysicBuilder", () => {
	let physicBuilder: StandardPhysicBuilder,
		standardPhysic: PhysicStub;
	
	beforeEach(() => {
		standardPhysic = new PhysicStub();
		spyOn(StandardPhysic, 'create').and.returnValue(standardPhysic);
	});
	
	it('should create a standardPhysic with elasticity', () => {
		physicBuilder = StandardPhysicBuilder.create();
		physicBuilder.withElasticity(0).build();
		expect(standardPhysic.setElasticity).toHaveBeenCalledWith(0);
	});
	
	it('should create a standardPhysic with friction', () => {
		physicBuilder = StandardPhysicBuilder.create();
		physicBuilder.withFriction(1).build();
		expect(standardPhysic.setFriction).toHaveBeenCalledWith(1);
	});
	
	it('should create a standardPhysic with friction', () => {
		physicBuilder = StandardPhysicBuilder.create();
		physicBuilder.withGravity(2).build();
		expect(standardPhysic.setGravity).toHaveBeenCalledWith(2);
	});
	
	it('should create a standardPhysic with speed', () => {
		physicBuilder = StandardPhysicBuilder.create();
		physicBuilder.withSpeed(3).build();
		expect(standardPhysic.setSpeed).toHaveBeenCalledWith(3);
	});
	
	it('should create a standardPhysic with a vector angle', () => {
		physicBuilder = StandardPhysicBuilder.create();
		physicBuilder.withAngle(4).build();
		expect(standardPhysic.setVectorAngle).toHaveBeenCalledWith(4);
	});
	
	it('should return the standard physic', () => {
		physicBuilder = StandardPhysicBuilder.create();
		expect(physicBuilder.build()).toBe(standardPhysic);
	});
});