import { UserInput } from "../../core/userInput/userInput";

export class UserInputStub implements UserInput {
	listenToEvent: (fn: (x: number, y: number) => void) => void = jasmine.createSpy("listenToEvent");
}