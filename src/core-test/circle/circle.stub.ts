export class CircleStub {
	public x: number;
	public y: number;
	public radius: number;
	public isOutsideBottomView: () => boolean = jasmine.createSpy('isOutsideBottomView');
	public positionToBottomView: () => void = jasmine.createSpy('positionToBottomView');
}