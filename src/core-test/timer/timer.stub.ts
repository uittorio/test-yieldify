import { Timer } from "../../core/timer/timer";
import { Updatable } from "../../core/updatable/updatable";

export class TimerStub implements Timer {
	listenTo: (updatable: Updatable) => void = jasmine.createSpy("listenTo");
}