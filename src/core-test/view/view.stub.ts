import { View } from "../../core/view/view";
import { Circle } from "../../core/circle/circle";

export class ViewStub implements View {
	public height: number;
	public width: number;
	public drawCircle: (circle: Circle) => void = jasmine.createSpy("drawCircle");
	public update: () => void = jasmine.createSpy("update");
}