import { IBallFactory } from "../../core/ball/ballFactory.interface";
import { IBall } from "../../core/ball/ball.interface";

export class BallFactoryStub implements IBallFactory {
	create: (x: number, y: number, radius: number) => IBall  = jasmine.createSpy("create");
}