import { IBall } from "../../core/ball/ball.interface";
import { Physic } from "../../core/physic/physic";

export class BallStub implements IBall {
	public radius: number;
	public x: number;
	public y: number;
	public setPhysic: (physic: Physic) => void = jasmine.createSpy("setPhysic");
	public update: () => void = jasmine.createSpy("update");
	public isOutsideBottomView: () => boolean = jasmine.createSpy("isOutsideBottomView");
	public positionToBottomView: () => void = jasmine.createSpy("positionToBottomView");
}