import { Ball } from "../../core/ball/ball";
import { ViewStub } from "../view/view.stub";
import { PhysicStub } from "../physic/physic.stub";

describe("Ball", () => {
	let ball: Ball,
		view: ViewStub,
		physic: PhysicStub,
		x2: number,
		y2: number;
	
	beforeEach(() => {
		view = new ViewStub();
		physic = new PhysicStub();
		ball = new Ball(view, 1, 2, 3);
		ball.setPhysic(physic);
	});
	
	it('should move the physic when updating', () => {
		ball.update();
		expect(physic.move).toHaveBeenCalledWith(ball);
	});
	
	it('should draw the circle when updating', () => {
		ball.update();
		expect(view.drawCircle).toHaveBeenCalledWith(ball);
	});
	
	describe("when is outside the bottom view", () => {
		it('should be outside the bottom view', () => {
			ball.y = 10;
			ball.radius = 20;
			view.height = 10;
			
			expect(ball.isOutsideBottomView()).toBe(true);
		});
	});
	
	describe("when is inside the bottom view", () => {
		it('should be inside the bottom view', () => {
			ball.y = 1;
			ball.radius = 2;
			view.height = 10;
			
			expect(ball.isOutsideBottomView()).toBe(false);
		});
	});
	
	describe("when positioning to the bottom view", () => {
		it('should move the ball to the bottom', () => {
			ball.y = 1;
			ball.radius = 2;
			view.height = 10;
			ball.positionToBottomView();
			
			expect(ball.y).toBe(8);
		});
	});
	
});