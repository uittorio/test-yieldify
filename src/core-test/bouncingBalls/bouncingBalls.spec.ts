import { ViewStub } from "../view/view.stub";
import { PhysicStub } from "../physic/physic.stub";
import { BouncingBalls } from "../../core/bouncingBalls/bouncingBalls";
import { TimerStub } from "../timer/timer.stub";
import { UserInputStub } from "../userInput/userInput.stub";
import { IBallFactory } from "../../core/ball/ballFactory.interface";
import { UserInput } from "../../core/userInput/userInput";
import { Timer } from "../../core/timer/timer";
import { View } from "../../core/view/view";
import { BallFactoryStub } from "../ball/ballFactory.stub";
import { PhysicBuilderStub } from "../physic/physicBuilder.stub";
import { PhysicBuilder } from "../../core/physic/physicBuilder";
import { IBall } from "../../core/ball/ball.interface";
import { BallStub } from "../ball/ball.stub";
import { Physic } from "../../core/physic/physic";

describe("BouncingBalls", () => {
	let bouncingBalls: BouncingBalls,
		view: View,
		timer: Timer,
		userInput: UserInput,
		ballFactory: IBallFactory,
		physicBuilder: PhysicBuilder,
		userInputCallBack: (x: number, y: number) => void,
		ballStub: IBall;
	
	beforeEach(() => {
		view = new ViewStub();
		timer = new TimerStub();
		userInput = new UserInputStub();
		ballFactory = new BallFactoryStub();
		physicBuilder = new PhysicBuilderStub();
		bouncingBalls = BouncingBalls.create(view, timer, userInput, ballFactory, physicBuilder);
		(userInput.listenToEvent as jasmine.Spy).and.callFake((fn: (x: number, y: number) => void) => {
			userInputCallBack = fn;
		});
		
		ballStub = new BallStub();
		(ballFactory.create as jasmine.Spy).and.returnValue(ballStub);
		bouncingBalls.start();
	});
	
	it('should add himself to the timer', () => {
		expect(timer.listenTo).toHaveBeenCalledWith(bouncingBalls);
	});
	
	it('should listen to userInputs', () => {
		expect(userInput.listenToEvent).toHaveBeenCalledWith(jasmine.any(Function));
	});
	
	describe("when userinput event", () => {
		let physic: Physic;
		
		beforeEach(() => {
			physic = new PhysicStub();
			(physicBuilder.build as jasmine.Spy).and.returnValue(physic);
			userInputCallBack(1, 2);
		});
		
		it("should create a new ball with configuration", () => {
			expect(ballFactory.create).toHaveBeenCalledWith(1, 2, 10);
		});
		
		it("should set the physic to the circle", () => {
			expect(physicBuilder.withElasticity).toHaveBeenCalledWith(1);
			expect(physicBuilder.withAngle).toHaveBeenCalledWith(jasmine.any(Number));
			expect(physicBuilder.withFriction).toHaveBeenCalledWith(0.008);
			expect(physicBuilder.withGravity).toHaveBeenCalledWith(0.1);
			expect(physicBuilder.withSpeed).toHaveBeenCalledWith(jasmine.any(Number));
			expect(ballStub.setPhysic).toHaveBeenCalledWith(physic);
		});
	});
	
	describe("when updating ", () => {
		let physic: Physic;
		
		beforeEach(() => {
			userInputCallBack(1, 2);
			userInputCallBack(2, 3);
			bouncingBalls.update();
		});
		
		it("should update the view", () => {
			expect(view.update).toHaveBeenCalledWith();
		});
		
		it("should update all the balls", () => {
			expect(ballStub.update).toHaveBeenCalledWith();
			expect((ballStub.update as jasmine.Spy).calls.count()).toBe(2);
		});
	});
});