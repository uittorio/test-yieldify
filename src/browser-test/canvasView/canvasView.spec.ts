import { CanvasRenderer } from "../../browser/canvasRenderer/canvasRenderer";
import { CanvasView } from "../../browser/canvasView/canvasView";
import { CanvasRendererStub } from "../canvasRenderer/canvasRenderer.stub";
import { IWindow } from "../../core/window/window";
import { CircleStub } from "../../core-test/circle/circle.stub";

describe("CanvasView", () => {
	let canvasView: CanvasView,
		canvasRenderer: CanvasRendererStub,
		window: IWindow;
	
	beforeEach(() => {
		canvasRenderer = new CanvasRendererStub();
		canvasRenderer.width = 100;
		canvasRenderer.height = 200;
		window = {
			innerWidth: 1000,
			innerHeight: 2000
		};
		canvasView = CanvasView.create(canvasRenderer as CanvasRenderer, window);
	});
	
	it('should be possible to get the height', () => {
		expect(canvasView.height).toBe(200);
	});
	
	it('should be possible to get the width', () => {
		expect(canvasView.width).toBe(100);
	});
	
	it('should clear the renderer when updating the view', () => {
		canvasView.update();
		expect(canvasRenderer.clear).toHaveBeenCalledWith();
	});
	
	it('should resize the renderer when updating the view with the latest window sizes', () => {
		canvasView.update();
		expect(canvasRenderer.resize).toHaveBeenCalledWith(1000, 2000);
	});
	
	it('should draw a circle from a circle', () => {
		let circle: CircleStub = new CircleStub();
		circle.x = 10;
		circle.y = 20;
		circle.radius = 25;
		canvasView.drawCircle(circle);
		expect(canvasRenderer.drawCircle).toHaveBeenCalledWith(10, 20, 25);
	});
});