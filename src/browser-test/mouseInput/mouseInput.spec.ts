import { MouseInput } from "../../browser/mouseInput/mouseInput";

describe("MouseInput", () => {
	let mouseInput: MouseInput,
		x: number,
		y: number,
		x2: number,
		y2: number;
	
	beforeEach(() => {
		mouseInput = MouseInput.create();
	});
	
	it('should listen to click event and get the coordinates', () => {
		mouseInput.listenToEvent((clientX: number, clientY: number) => {
			x = clientX;
			y = clientY;
		});
		document.documentElement.click();
		expect(x).toBe(0);
		expect(y).toBe(0);
	});
	
	it('should listen for multiple events', () => {
		mouseInput.listenToEvent((clientX: number, clientY: number) => {
			x = clientX;
			y = clientY;
		});
		
		mouseInput.listenToEvent((clientX: number, clientY: number) => {
			x2 = clientX;
			y2 = clientY;
		});
		document.documentElement.click();
		expect(x2).toBe(0);
		expect(y2).toBe(0);
	});
});