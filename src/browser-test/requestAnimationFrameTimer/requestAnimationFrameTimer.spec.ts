import { RequestAnimationFrameTimer } from "../../browser/requestAnimationFrameTimer/requestAnimationFrameTimer";

describe("RequestAnimationFrameTimer", () => {
	let requestAnimationFrameTimer: RequestAnimationFrameTimer,
		updateFn: () => void;
	
	beforeEach(() => {
		spyOn(window, 'requestAnimationFrame').and.callThrough();
		
		updateFn = jasmine.createSpy('update');
		requestAnimationFrameTimer = RequestAnimationFrameTimer.create();
		requestAnimationFrameTimer.listenTo({
			update: updateFn
		});
	});
	
	it('should use request animation frame strategy', () => {
		expect(window.requestAnimationFrame).toHaveBeenCalled();
		setTimeout(() => {
			expect(updateFn).toHaveBeenCalled();
		}, 0);
	});
});