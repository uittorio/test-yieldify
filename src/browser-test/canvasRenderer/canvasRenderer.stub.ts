export class CanvasRendererStub {
	public width: number;
	public height: number;
	public drawCircle: (x: number, y: number, radius: number) => void = jasmine.createSpy('drawCircle');
	public clear: () => void = jasmine.createSpy('clear');
	public resize: (width: number, height: number) => void = jasmine.createSpy('resize');
}