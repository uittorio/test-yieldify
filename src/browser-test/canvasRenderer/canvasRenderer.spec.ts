import { CanvasRenderer } from "../../browser/canvasRenderer/canvasRenderer";
import { HtmlCanvasElementStub } from "../htmlCanvasElement/htmlCanvasElement.stub";
import { CanvasRenderingContext2DStub } from "../canvasRenderingContext2D/canvasRenderingContext2D.stub";

describe("CanvasRendered", () => {
	let canvasRenderer: CanvasRenderer,
		htmlCanvasElement: HtmlCanvasElementStub,
		canvasContext: CanvasRenderingContext2DStub;
	
	beforeEach(() => {
		htmlCanvasElement = new HtmlCanvasElementStub();
		htmlCanvasElement.width = 100;
		htmlCanvasElement.height = 200;
		canvasContext = new CanvasRenderingContext2DStub();
		(htmlCanvasElement.getContext as jasmine.Spy).and.returnValue(canvasContext);
		canvasRenderer = CanvasRenderer.create(htmlCanvasElement as HTMLCanvasElement);
	});
	
	it('should get the 2d context', () => {
		expect(htmlCanvasElement.getContext).toHaveBeenCalledWith("2d");
	});
	
	it('should be able to draw a black circle', () => {
		canvasRenderer.drawCircle(3, 2, 10);
		expect(canvasContext.fillStyle).toBe("black");
		expect(canvasContext.beginPath).toHaveBeenCalledWith();
		expect(canvasContext.closePath).toHaveBeenCalledWith();
		expect(canvasContext.fill).toHaveBeenCalledWith();
		expect(canvasContext.arc).toHaveBeenCalledWith(3, 2, 10, 0, 2 * Math.PI);
	});
	
	it('should be able to clear the content', () => {
		canvasRenderer.clear();
		expect(canvasContext.clearRect).toHaveBeenCalledWith(0, 0, 100, 200);
	});
	
	it('should be able to resize the content', () => {
		expect(canvasContext.canvas.width).toBe(0);
		expect(canvasContext.canvas.height).toBe(0);
		canvasRenderer.resize(400, 600);
		expect(canvasContext.canvas.width).toBe(400);
		expect(canvasContext.canvas.height).toBe(600);
	});
	
	it('should have the height', () => {
		expect(canvasRenderer.width).toBe(100);
		expect(canvasRenderer.height).toBe(200);
	});
});