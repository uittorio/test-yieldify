import { CanvasRenderingContext2DStub } from "../canvasRenderingContext2D/canvasRenderingContext2D.stub";

export class HtmlCanvasElementStub {
	public getContext: (type: string) => CanvasRenderingContext2DStub = jasmine.createSpy('getContext');
	public width: number = 0
	public height: number = 0;
}