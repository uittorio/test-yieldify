export class CanvasRenderingContext2DStub {
	public fillStyle: string = "";
	public canvas: {
		width: number;
		height: number;
	};
	public clearRect: (x: number, y: number, w: number, h: number) => void = jasmine.createSpy('clearRect');
	public beginPath: () => void = jasmine.createSpy('beginPath');
	public closePath: () => void = jasmine.createSpy('closePath');
	public fill: () => void = jasmine.createSpy('fill');
	public arc: (x: number, y: number, radius: number, startAngle: number, endAngle: number) => void = jasmine.createSpy('arc');
	
	constructor() {
		this.fillStyle = "";
		this.canvas = {
			width: 0,
			height: 0
		};
	}
}