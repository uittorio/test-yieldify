import { Circle } from "../circle/circle";
import { Updatable } from "../updatable/updatable";

export interface View extends Updatable {
	height: number;
	width: number;
	drawCircle(circle: Circle): void;
}