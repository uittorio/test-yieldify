export interface Shape {
	x: number;
	y: number;
	isOutsideBottomView(): boolean;
	positionToBottomView(): void;
}