export interface IWindow {
	innerWidth: number;
	innerHeight: number;
}