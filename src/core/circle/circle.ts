import { Shape } from "../shape/shape";

export interface Circle extends Shape {
	radius: number;
}