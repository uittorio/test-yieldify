export interface UserInput {
	listenToEvent(fn: (x: number, y: number) => void): void;
}