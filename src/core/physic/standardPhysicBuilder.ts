import { Physic } from "./physic";
import { View } from "../view/view";
import { StandardPhysic } from "./standardPhysic";
import { PhysicBuilder } from "./physicBuilder";

export class StandardPhysicBuilder implements PhysicBuilder {
	private _elasticity: number;
	private _friction: number;
	private _gravity: number;
	private _speed: any;
	private _angle: number;
	
	private constructor() {}
	
	public static create(): StandardPhysicBuilder {
		return new StandardPhysicBuilder();
	}
	
	public withElasticity(elasticity: number): StandardPhysicBuilder {
		this._elasticity = elasticity;
		return this;
	}
	
	public withFriction(friction: number): StandardPhysicBuilder {
		this._friction = friction;
		return this;
	}
	
	public withGravity(gravity: number): StandardPhysicBuilder {
		this._gravity = gravity;
		return this;
	}
	
	public withSpeed(speed: number): StandardPhysicBuilder {
		this._speed = speed;
		return this;
	}
	
	public withAngle(angle: number): StandardPhysicBuilder {
		this._angle = angle;
		return this;
	}
	
	public build(): Physic {
		let physic = StandardPhysic.create();
		physic.setElasticity(this._elasticity);
		physic.setFriction(this._friction);
		physic.setGravity(this._gravity);
		physic.setSpeed(this._speed);
		physic.setVectorAngle(this._angle);
		return physic;
	}
	
}