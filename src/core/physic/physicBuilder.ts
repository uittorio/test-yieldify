import { Physic } from "./physic";

export interface PhysicBuilder {
	withElasticity(elasticity: number): PhysicBuilder
	
	withFriction(friction: number): PhysicBuilder
	
	withGravity(gravity: number): PhysicBuilder
	
	withSpeed(speed: number): PhysicBuilder
	
	withAngle(angle: number): PhysicBuilder
	
	build(): Physic
}