import { Shape } from "../shape/shape";
import { Physic } from "./physic";

export class StandardPhysic implements Physic {
	private _elasticity: number;
	private _friction: number;
	private _gravity: number;
	private _speed: number;
	private _velocityX: number;
	private _velocityY: number;
	
	private constructor() {}
	
	public static create() {
		return new StandardPhysic();
	}
	
	/* set this value to define how much it will bounce before stopping */
	public setElasticity(elasticity: number) {
		this._elasticity = elasticity;
	}
	
	/* set this value to define how fast will stop to move horizontally */
	public setFriction(friction: number) {
		this._friction = friction;
	}
	
	/* set this value to define how fast will go down */
	public setGravity(gravity: number) {
		this._gravity = gravity;
	}
	
	/* set this value to define the initial velocity of his direction*/
	public setSpeed(speed: number) {
		this._speed = speed;
	}
	
	/* Set this value to decide the angle of your shape direction vector */
	
	/* This method will calculate the vector based on an angle
	 * The velocity x is needed to know where the shape should go horizontally
	 * The velocity x is needed to know where the shape should go vertically */
	public setVectorAngle(angle: number) {
		let radians: number = angle * Math.PI / 180;
		this._velocityX = Math.cos(radians) * this._speed;
		this._velocityY = Math.sin(radians) * this._speed;
	}
	
	/* This method will move the shape
	 * every cycle it will remove the gravity from the velocity so the shape will go slowly down
	 * every cycle it will use the friction to slowly stop the shape horizontal movement
	 * if the shape is outside the bottom view it will prevent it. its not handling the left,top,right corners.
	 * */
	
	public move(shape: Shape) {
		this._velocityY += this._gravity;
		this._velocityX = this._velocityX - (this._velocityX * this._friction);
		
		shape.x += this._velocityX;
		shape.y += this._velocityY;
		
		if (shape.isOutsideBottomView()) {
			shape.positionToBottomView();
			this._velocityY = -(this._velocityY - this._elasticity);
		}
	}
}
