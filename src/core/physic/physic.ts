import { Shape } from "../shape/shape";

export interface Physic {
	move(shape: Shape): void;
	setElasticity(elasticity: number): void
	setFriction(friction: number): void
	setGravity(gravity: number): void
	setSpeed(speed: number): void
	setVectorAngle(angle: number): void
}