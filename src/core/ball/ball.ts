import { Physic } from "../physic/physic";
import { View } from "../view/view";
import { IBall } from "./ball.interface";

export class Ball implements IBall {
	public radius: number;
	public y: number;
	public x: number;
	private _view: View;
	private _physic: Physic;
	
	constructor(view: View, x: number, y: number, radius: number) {
		this._view = view;
		this.x = x;
		this.y = y;
		this.radius = radius;
	}
	
	public setPhysic(physic: Physic) {
		this._physic = physic;
	}
	
	public update(): void {
		this._physic.move(this);
		this._view.drawCircle(this);
	}
	
	public isOutsideBottomView(): boolean {
		return (this.y + this.radius) > this._view.height;
	}
	
	public positionToBottomView(): void {
		this.y = this._view.height - this.radius;
	}
}