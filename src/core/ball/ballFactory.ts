import { Ball } from "./ball";
import { View } from "../view/view";
import { IBallFactory } from "./ballFactory.interface";

export class BallFactory implements IBallFactory {
	private _view: View;
	
	private constructor(view: View) {
		this._view = view;
	}
	
	public static create(view: View) {
		return new BallFactory(view);
	}
	
	public create(x: number, y: number, radius: number): Ball {
		return new Ball(this._view, x, y, radius);
	}
}