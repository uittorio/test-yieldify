import { Updatable } from "../updatable/updatable";
import { Circle } from "../circle/circle";
import { Physic } from "../physic/physic";

export interface IBall extends Updatable, Circle {
	setPhysic(physic: Physic): void;
}