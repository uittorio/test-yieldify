import { IBall } from "./ball.interface";

export interface IBallFactory {
	create(x: number, y: number, radius: number): IBall;
}
