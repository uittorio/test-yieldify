import { Updatable } from "../updatable/updatable";

export interface Timer {
	listenTo(updatable: Updatable): void;
}