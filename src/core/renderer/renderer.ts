export interface Renderer {
	drawCircle(x: number, y: number, radius: number): void;
	clear(x: number, y: number, radius: number): void;
	resize(width: number, height: number): void;
}