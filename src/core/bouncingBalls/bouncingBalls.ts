import { Timer } from "../timer/timer";
import { View } from "../view/view";
import { UserInput } from "../userInput/userInput";
import { RADIUS } from "../radius/radius";
import { Updatable } from "../updatable/updatable";
import { PhysicBuilder } from "../physic/physicBuilder";
import { IBall } from "../ball/ball.interface";
import { IBallFactory } from "../ball/ballFactory.interface";

export class BouncingBalls {
	private _timer: Timer;
	private _view: View;
	private _userInput: UserInput;
	private _balls: Array<IBall> = [];
	private _ballFactory: IBallFactory;
	private _physicBuilder: PhysicBuilder;
	
	private constructor(view: View, timer: Timer, userInput: UserInput, ballFactory: IBallFactory, physicBuilder: PhysicBuilder) {
		this._view = view;
		this._timer = timer;
		this._userInput = userInput;
		this._ballFactory = ballFactory;
		this._physicBuilder = physicBuilder;
	}
	
	public static create(view: View, timer: Timer, userInput: UserInput, ballFactory: IBallFactory, physicBuilder: PhysicBuilder): BouncingBalls {
		return new BouncingBalls(view, timer, userInput, ballFactory, physicBuilder);
	}
	
	public start(): void {
		this._timer.listenTo(this);
		this._userInput.listenToEvent((x: number, y: number) => this._createBall(x, y));
	}
	
	public update(): void {
		this._view.update();
		
		this._balls.forEach(((ball: Updatable) => {
			ball.update();
		}));
	}
	
	private _createBall(x: number, y: number): void {
		let ball: IBall = this._ballFactory.create(x, y, RADIUS);
		let friction: number = 0.008;
		let gravity: number = 0.1;
		
		let physic = this._physicBuilder.withElasticity(1)
			.withAngle(this._generateRandomAngle())
			.withFriction(friction)
			.withGravity(gravity)
			.withSpeed(this._generateRandomSpeed())
			.build();
		
		ball.setPhysic(physic);
		
		this._balls.push(ball);
	}
	
	private _generateRandomAngle(): number {
		return Math.random() * 360;
	}
	
	private _generateRandomSpeed(): number {
		return Math.random() * (6 - 4) + 4;
	}
}