import { View } from "../../core/view/view";
import { CanvasRenderer } from "../canvasRenderer/canvasRenderer";
import { IWindow } from "../../core/window/window";
import { Circle } from "../../core/circle/circle";

export class CanvasView implements View {
	private _canvasRenderer: CanvasRenderer;
	private _window: IWindow;
	
	public static create(canvasRenderer: CanvasRenderer, window: IWindow): CanvasView {
		return new CanvasView(canvasRenderer, window);
	}
	
	private constructor(canvasRenderer: CanvasRenderer, window: IWindow) {
		this._canvasRenderer = canvasRenderer;
		this._window = window;
	}
	
	public get height(): number {
		return this._canvasRenderer.height;
	};
	
	public get width(): number {
		return this._canvasRenderer.width;
	}
	
	public update(): void {
		this._canvasRenderer.clear();
		this._canvasRenderer.resize(this._window.innerWidth, this._window.innerHeight);
	}
	
	public drawCircle(circle: Circle): void {
		this._canvasRenderer.drawCircle(circle.x, circle.y, circle.radius);
	}
}