import { Timer } from "../../core/timer/timer";
import { Updatable } from "../../core/updatable/updatable";

export class RequestAnimationFrameTimer implements Timer {
	private _updatable: Updatable;
	
	private constructor() {}
	
	public static create(): RequestAnimationFrameTimer {
		return new RequestAnimationFrameTimer();
	}
	
	public listenTo(updatable: Updatable) {
		this._updatable = updatable;
		window.requestAnimationFrame(this._tick.bind(this));
	}
	
	private _tick() {
		this._updatable.update();
		window.requestAnimationFrame(this._tick.bind(this));
	}
}