import { Renderer } from "../../core/renderer/renderer";

export class CanvasRenderer implements Renderer {
	private _htmlCanvasElement: HTMLCanvasElement;
	private _context: CanvasRenderingContext2D;
	
	public static create(htmlCanvasElement: HTMLCanvasElement): CanvasRenderer {
		return new CanvasRenderer(htmlCanvasElement);
	}
	
	private constructor(htmlCanvasElement: HTMLCanvasElement) {
		this._htmlCanvasElement = htmlCanvasElement;
		this._context = this._htmlCanvasElement.getContext("2d");
	}
	
	public drawCircle(x, y, radius) {
		this._context.fillStyle = "black";
		
		this._context.beginPath();
		this._context.arc(x, y, radius, 0, 2 * Math.PI);
		this._context.fill();
		this._context.closePath();
	}
	
	public clear() {
		this._context.clearRect(0, 0, this._htmlCanvasElement.width, this._htmlCanvasElement.height);
	}
	
	public resize(width, height) {
		this._context.canvas.width = width;
		this._context.canvas.height = height;
	}
	
	public get height(): number {
		return this._htmlCanvasElement.height;
	}
	
	public get width(): number {
		return this._htmlCanvasElement.width;
	}
}