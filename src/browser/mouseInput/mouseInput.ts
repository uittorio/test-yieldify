import { UserInput } from "../../core/userInput/userInput";

export class MouseInput implements UserInput {
	private _listener: Array<(x: number, y: number) => void> = [];
	
	private constructor() {
		document.addEventListener("click", (event: MouseEvent) => {
			this._listener.forEach((listener) => {
				listener(event.clientX, event.clientY);
			});
		});
	}
	
	public static create(): MouseInput {
		return new MouseInput();
	}
	
	public listenToEvent(fn: (x: number, y: number) => void): void {
		this._listener.push((x: number, y: number) => fn(x, y));
	}
}
