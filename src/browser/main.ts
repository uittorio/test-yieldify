import { BouncingBalls } from "../core/bouncingBalls/bouncingBalls";
import { CanvasView } from "./canvasView/canvasView";
import { CanvasRenderer } from "./canvasRenderer/canvasRenderer";
import { RequestAnimationFrameTimer } from "./requestAnimationFrameTimer/requestAnimationFrameTimer";
import { MouseInput } from "./mouseInput/mouseInput";
import { BallFactory } from "../core/ball/ballFactory";
import { PhysicBuilder } from "../core/physic/physicBuilder";
import { StandardPhysicBuilder } from "../core/physic/standardPhysicBuilder";
import { IBallFactory } from "../core/ball/ballFactory.interface";
import { UserInput } from "../core/userInput/userInput";

document.addEventListener("DOMContentLoaded", () => {
		let htmlCanvas = document.getElementById("canvas");
		let canvasRenderer: CanvasRenderer = CanvasRenderer.create(htmlCanvas as HTMLCanvasElement);
		let canvasView: CanvasView = CanvasView.create(canvasRenderer, window);
		let timer: RequestAnimationFrameTimer = RequestAnimationFrameTimer.create();
		let userInput: UserInput = MouseInput.create();
		let ballFactory: IBallFactory = BallFactory.create(canvasView);
		let physic: PhysicBuilder = StandardPhysicBuilder.create();

		BouncingBalls.create(canvasView, timer, userInput, ballFactory, physic).start();
});