# README #

### Setup ###
install dependencies before starting
```
npm install
```

### Test ###
```
npm run test
```

### Dev ###
to see the app not minified
```
npm start
```
npm run dev

### Build ###
to see the app minified
```
npm run build
```