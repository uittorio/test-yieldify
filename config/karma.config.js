const webpackConfig = require('./webpack.common.config');

module.exports = function(config) {
  var _config = {
    basePath: '',
    frameworks: ['jasmine'],
    files: [
      { pattern: './webpack.test.context.js', watched: false }
    ],
    preprocessors: {
      './webpack.test.context.js': ['webpack']
    },
    webpack: webpackConfig,
    webpackServer: {
      noInfo: true
    },
    reporters: ['progress'],
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: false,
    browsers: ['ChromeHeadless'],
    singleRun: true
  };

  config.set(_config);
};
