/*globals require, module */

const root = require('./helpers/root'),
  webpackMerge = require('webpack-merge'),
  commonConfig = require('./webpack.common.config.js');

module.exports = webpackMerge(commonConfig, {
  output: {
    path: root('build'),
    filename: '[name].[hash].bundle.js'
  },
  devServer: {
    stats: {
      colors: false,
      hash: false,
      version: false,
      timings: false,
      assets: true,
      chunks: false,
      modules: false,
      reasons: false,
      children: false,
      source: false,
      errors: true,
      errorDetails: false,
      warnings: false,
      publicPath: false
    },
    port: 8080,
    host: '0.0.0.0',
    watchOptions: {
      aggregateTimeout: 300,
      poll: 1000
    },
    contentBase: root('build')
  },
  devtool: 'eval-source-map'
});
