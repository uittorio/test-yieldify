/*globals require, module */

const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  module: {
    rules: [
      {
        test: /\.ts$/,
        loaders: [
          'awesome-typescript-loader'
        ]
      }
    ]
  },
  entry: {
    app: ['./src/browser/main.ts']
  },
  resolve: {
    extensions: ['.js', '.ts']
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: 'src/browser/index.html'
    }),
    new CopyWebpackPlugin([
      {
        from: 'src/browser/styles',
        to: '.'
      }
    ])
  ]
};
